/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Settings.cpp
 * Author: user
 * 
 * Created on 21 сентября 2018 г., 12:42
 */

#include "Settings.h"
#include <fstream>

Settings setts;

Settings::Settings() {
    path = "/etc/mosbdu/";
}

Settings::Settings(const Settings& orig) {
}

Settings::~Settings() {
}

string Settings::read_string(string name){
    ifstream f(path+name);
    string value = "";
    f >> value;
    return value;
}

void Settings::write_string(string name, string value){
    ofstream f(path+name);
    f << value;
}

vector<string> Settings::read_strings(string name){
    ifstream f(path+name);
    vector<string> values;
    
    if(!f.good())
        return values;
    
    while(!f.eof()){
        string s;
        f >> s;
        values.push_back(s);
    }
    return values;
}

void Settings::write_strings(string name, vector<string> values){
    ofstream f(path+name);
    for(string s : values)
        f << s << endl;
}

int Settings::read_int(string name){
    ifstream f(path+name);
    int value = 0;
    f >> value;
    return value;
}

void Settings::write_int(string name, int value){
    ofstream f(path+name);
    f << value;
}

vector<int> Settings::read_ints(string name){
    vector<string> strs = read_strings(name);
    vector<int> ints;
    for(string str : strs){
        if(str != ""){
            int n = stoi(str);
            ints.push_back(n);
        }else{
            ints.push_back(-1);
        }
    }
    return ints;
}

void Settings::write_ints(string name, vector<int> ints){
    ofstream f(path+name);
    for(int n : ints)
        f << n << endl;
}