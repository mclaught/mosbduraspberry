/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPChannel.h
 * Author: user
 *
 * Created on 19 сентября 2018 г., 23:28
 */

#ifndef UDPCHANNEL_H
#define UDPCHANNEL_H

#include <string>
#include "Channel.h"

using namespace std;

class UDPChannel : public Channel {
public:
    string error;
    
    UDPChannel();
    UDPChannel(string iface, string multicastAddr, string clientName);
    UDPChannel(const UDPChannel& orig);
    virtual ~UDPChannel();
    
    virtual void onRead();
    virtual void onError();
    void searchServer();
private:
    string iface;
    string multicastAddr;
    string clientName;
    bool doSearch = false;

    virtual void sendData(string data);
    virtual void sendDataTo(string data, sockaddr_in addr);
    virtual void onTime();
    void stopSearch();
};

#endif /* UDPCHANNEL_H */

