/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Channel.cpp
 * Author: user
 * 
 * Created on 19 сентября 2018 г., 23:13
 */

#include <iostream>
#include "Channel.h"

Channel::Channel() : MyObject() {
}

Channel::Channel(const Channel& orig) {
}

Channel::~Channel() {
}

void Channel::sendRequest(CmdParams params){
    XMLDocument xml;
    XMLElement* root = xml.NewElement("REQUEST");

    for (auto prm : params) {
        
        root->SetAttribute(prm.first.c_str(), prm.second.c_str());
    }
    xml.LinkEndChild(root);

    XMLPrinter printer(nullptr, true);
    xml.Print(&printer);

    sendData(const_cast<char*>(printer.CStr()));
    //sendAsync(printer.CStr());
}

void Channel::sendResponse(XMLElement *xeRequest, sockaddr_in addr, CmdParams addParams){
    XMLDocument xml;

    XMLElement* root = xml.NewElement("RESPONSE");

    for(XMLAttribute* attr = const_cast<XMLAttribute*>(xeRequest->FirstAttribute()); attr; attr = const_cast<XMLAttribute*>(attr->Next())){
        if(addParams.find(attr->Name()) == addParams.end())
            root->SetAttribute(attr->Name(), attr->Value());
    }

    for (auto prm : addParams) {
        root->SetAttribute(prm.first.c_str(), prm.second.c_str());
    }
    xml.LinkEndChild(root);

    XMLPrinter printer(nullptr, true);
    xml.Print(&printer);

    sendDataTo(const_cast<char*>(printer.CStr()), addr);
    //sendAsync(printer.CStr(), addr);
}

bool Channel::checkCommand(XMLElement *xe, string command, string action){
    if(!xe->Attribute("command", command.c_str()))
        return false;

    if(action!="" && !xe->Attribute("action", action.c_str()))
        return false;

    return  true;
}

void Channel::run(){
//    if(active){
//        fd_set rd_set, wr_set, *pwr_set = nullptr;
//        FD_SET(sock, &rd_set);
//        timeval tv = {0, 50000};
//
//        int cnt = select(sock+1, &rd_set, nullptr, nullptr, &tv);
//        if(cnt == -1){
//            cerr << name << " select: " << strerror(errno) << endl;
//        }else if(cnt > 0){
//            if(FD_ISSET(sock, &rd_set)){
//                onRead();
//            }
//        }
//    }
    
    struct timespec ts;
    if(clock_gettime(CLOCK_MONOTONIC,&ts) != 0) {
        cerr << "Clock error" << endl;
        return;
    }
    uint32_t tm = ts.tv_sec*1000 + ts.tv_nsec/1000000;
    if((tm - last_tm) > 10000){
        onTime();
        last_tm = tm;
    }
}

void Channel::sendAsync(string data){
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    sendAsync(data, addr);
}

void Channel::sendAsync(string data, sockaddr_in addr){
    send_queue[send_queue_in].data = data;
    send_queue[send_queue_in].addr = addr;
    send_queue_in = (send_queue_in+1)%SEND_QUEUE_SZ;
}
