/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Channel.h
 * Author: user
 *
 * Created on 19 сентября 2018 г., 23:13
 */

#ifndef CHANNEL_H
#define CHANNEL_H

#include "MyObject.h"
#include <string>
#include <map>
#include "tinyxml2.h"
#include <netinet/in.h>

#define SEND_QUEUE_SZ   10

using namespace std;
using namespace tinyxml2;

class Channel : public MyObject {
public:
    typedef map<string, string> CmdParams;
    struct send_queue_iten_t{
        string data;
        sockaddr_in addr;
    };
    
    int sock = 0;
    string name;
    bool active = false;
    
    Channel();
    Channel(const Channel& orig);
    virtual ~Channel();
    
    void sendRequest(CmdParams params);
    void sendResponse(XMLElement *xeRequest, sockaddr_in addr, CmdParams addParams = CmdParams());
    bool checkCommand(XMLElement *xe, string command, string action);
    virtual void sendData(string data) = 0;
    virtual void sendDataTo(string data, sockaddr_in addr) = 0;
    virtual void onTime() = 0;
    virtual void onRead() = 0;
    virtual void onError() = 0;
    virtual void run();
    
private:
    uint32_t last_tm;
    send_queue_iten_t send_queue[SEND_QUEUE_SZ];
    int send_queue_in = 0;
    int send_queue_out = 0;
    
    void sendAsync(string data);
    void sendAsync(string data, sockaddr_in addr);
    
protected:
};

#endif /* CHANNEL_H */

