/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainModel.cpp
 * Author: user
 * 
 * Created on 19 сентября 2018 г., 19:39
 */

#include "MainModel.h"
#include <mpg123.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netdb.h>
#include "messages.h"
#include <iostream>
#include "Settings.h"
#include "UARTThread.h"
#include <vector>
#include <sys/poll.h>
#include "log.h"

//#define CLIENT "US-MUS.03.02.2F"
//#define IFACE "192.168.1.1"
//#define USE_POLL

MainModel::MainModel() : MyObject() {
    
    string mcast = setts.read_string("udp-multicast");
    if(mcast == ""){
        cerr << "Params error: no multicast address";
        exit(EXIT_FAILURE);
    }
    
    targets = setts.read_strings("targets");
    tgtName = "unknown";
    for(string target : targets){
        if(target != "" && target != "-"){
            if(tgtName == "unknown")
                tgtName = target;
            targets_set.insert(target);
        }
    }
    
    string server_addr = setts.read_string("server-addr");
    uint16_t server_port = setts.read_int("server-port");
    cout << "Server: " << server_addr << ":" << server_port << endl;
    
    //If addrs
    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char host[NI_MAXHOST];
    
    if (getifaddrs(&ifaddr) == -1)
    {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }
    
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
        if (ifa->ifa_addr == NULL)
            continue;
        
        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        if(ifa->ifa_addr->sa_family==AF_INET){
            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                break;
            }
            
            if(string(ifa->ifa_name) == "eth0"){
                cout << "Iface: " << ifa->ifa_name << " : " << host << endl;
                iface = host;
                break;
            }
        }
    }
    
    
    udp = new UDPChannel(iface, mcast, tgtName);
    udp->join(MSG_SERVER_FOUND, [this](void* params){
        sockaddr_in *addr = (sockaddr_in*)params;
        string s_addr = inet_ntoa(addr->sin_addr);
        uint16_t s_port = ntohs(addr->sin_port);
        for(TCPChannel *tcp : tcps){
            tcp->setServerParams(s_addr, s_port);
        }
        setts.write_string("server-addr", s_addr);
        setts.write_int("server-port", s_port);
    });
    channels.push_back(udp);
    
    for(string target : targets_set){
        if(target != "" && target != "-"){
            TCPChannel *tcp = new TCPChannel(target);
            tcp->setServerParams(server_addr, server_port);
            tcps.insert(tcp);
            channels.push_back(tcp);
        }
    }
    
    for(TCPChannel *tcp : tcps){
        tcp->join(MSG_START_SESSION, [this](void* params){
            uint32_t id = ((SessionsPrms*)params)->id;
            string url = ((SessionsPrms*)params)->url;
            string cliName = ((SessionsPrms*)params)->clientName;
            
            targets_t tgtMask = findTargets(cliName);
            if(tgtMask == 0)
                return;
            
            PSession sess = findSession(id);
            if(!sess.get()){
#ifndef SMART_PTR
                for(auto s : sessions)
                    delete s->second;
#endif
                
                sessions.clear();
                
                PSession new_sess = PSession(new Session(id, url, iface));
                cout << "Add new session "<< id <<". URL: " << url << endl;
                if(new_sess->start()){
                    sessions[id] = new_sess;
                    sess = new_sess;
                }
            }
            if(sess.get()){
                sess->addTargets(tgtMask);
                uart_thread->setTargets(sess->getTargets());//
                cout << "Add " << cliName <<" to " << id << endl;
            }
        });
        tcp->join(MSG_FINISH_SESSION, [this](void* params){
            uint32_t id = ((SessionsPrms*)params)->id;
            string clientName = ((SessionsPrms*)params)->clientName;
            
            targets_t tgtMask = findTargets(clientName);
            if(tgtMask == 0)
                return;
            
            PSession sess = findSession(id);
            if(sess){
                sess->removeTargets(tgtMask);
                uart_thread->setTargets(sess->getTargets());//
                cout << "Remove " << clientName <<" from " << id << endl;
                if(sess->getTargets() == 0){
#ifndef SMART_PTR
                    delete sess;
#endif
                    sess->stop();
                    sessions.erase(id);
                    
                    cout << "Remove session " << id << endl;
                }
            }
        });
    }
}

MainModel::MainModel(const MainModel& orig) : MyObject() {
}

MainModel::~MainModel() {
}

void MainModel::run(){
#ifdef USE_POLL
    pollfd fds[channels.size()];
    int i=0;
    for(Channel *ch : channels){
        fds[i].fd = ch->sock;
        fds[i].events = POLLIN;
        fds[i].revents = 0;
        i++;
    }
#endif    
    
    while(true){
#ifdef USE_POLL
        i=0;
        for(Channel *ch : channels){
            fds[i].fd = ch->sock;
            fds[i].events = POLLIN;
            fds[i].revents = 0;
            i++;
        }
        
        int cnt = poll(fds, channels.size(), 1000);
        if(cnt > 0){
            i=0;
            for(Channel *ch : channels){
                if(ch->active && (fds[i].revents & POLLIN)){
                    ch->onRead();
                }
                i++;
            }
        }else if(cnt == -1){
            cerr << strerror(errno) << endl;
        }
#else
        
        fd_set rd_set, er_set;
        timeval tv = {1, 0};
        
        int max_sock = 0;
        for(Channel *ch : channels){
            if(ch->active){
                FD_SET(ch->sock, &rd_set);
                FD_SET(ch->sock, &er_set);
                max_sock = std::max(max_sock, ch->sock);
            }
        }
        int cnt = select(max_sock+1, &rd_set, nullptr, &er_set, &tv);
        if(cnt == -1){
            cerr << red  << " select: " << strerror(errno) << clrst << endl;
            for(Channel *ch : channels){
                if(dynamic_cast<TCPChannel*>(ch) && ch->active){// && !checkSocket(ch->sock)
                    ch->onError();
                }
            }
        }else if(cnt>0){
            for(Channel *ch : channels){
                if(ch->active){
                    if(FD_ISSET(ch->sock, &rd_set))
                        ch->onRead();
                    if(FD_ISSET(ch->sock, &er_set))
                        ch->onError();
                }
            }
        }else{
            //cout << "Idle" << endl;
        }
#endif
        
        udp->run();
        
        for(TCPChannel *tcp : tcps){
            if(!tcp->isConfigured())
                udp->searchServer();
            
            if(tcp->state == TCPChannel::DISCONNECTED)
                tcp->connectServer();
            
            tcp->run();
        }
        
//        for(auto s : sessions){
//            s.second->run();
//        }
    }
}

PSession MainModel::findSession(uint32_t id){
    Sessions::iterator fnd = sessions.find(id);
    if(fnd != sessions.end())
        return fnd->second;
    else
        return PSession(nullptr);
}

vector<string> MainModel::splitString(string str){
    return vector<string>();
}

targets_t MainModel::findTargets(string tgt){
    int i = 0;
    targets_t mask = 0;
    for(auto t : targets){
        if(t == tgt){
            mask |= (1<<i);;
        }
        i++;
    }
    return mask;
}

bool MainModel::checkSocket(int sock){
    fd_set rd_set;
    timeval tv = {0, 10};
    FD_SET(sock, &rd_set);
    int res = select(sock+1, &rd_set, nullptr, nullptr, &tv);
    if(res == -1 && errno == EBADF){
        cerr << red << "socket " << sock << " is bad" << clrst << endl;
        return false;
    }else{
        return true;
    }
}