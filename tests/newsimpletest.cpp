/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   newsimpletest.cpp
 * Author: user
 *
 * Created on 20 сентября 2018 г., 12:09
 */

#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include "Session.h"
#include "TCPChannel.h"

#include "mpg123-1.25.10/src/libmpg123/mpg123.h"
#include "MyPlayer.h"

/*
 * Simple C++ Test Suite
 */

MyPlayer *player;

void test1() {
    std::cout << "newsimpletest test 1" << std::endl;
    
    Session *sess = new Session(100, "rtp://225.0.10.150:5544", "192.168.1.1");
}

//void test2() {
//    std::cout << "newsimpletest test 2" << std::endl;
//    
//    mpg123_init();
//
//    int error;
//    mpg123_handle* mh = mpg123_new(NULL, &error);
//    std::cout << mpg123_plain_strerror(error) << std::endl;
//    
//    FILE *f = fopen("test.bin", "rb");
//    if(!f){
//        std::cout << "%TEST_FAILED% time=0 testname=test2 (newsimpletest) message=File not found" << std::endl;
//    }
//    
//    int fsz = 5*1024*1024;
//    char buf[fsz];
//    char *p = buf;
//    fsz = 0;
//    while(!feof(f)){
//        int sz = fread(buf, 1, 1024, f);
//        p += sz;
//        fsz += sz;
//    }
//    if(fsz == 0){
//        std::cerr << strerror(ferror(f)) << std::endl;
//        return;
//    }
//    
//    int err = mpg123_open_feed(mh);
//    if(err){
//        std::cerr << mpg123_plain_strerror(err) << std::endl;
//        return;
//    }
//    
//    char* p1 = buf+16;
//    
//    for(int i = 16; i<fsz; i++){
//        RTP* p2 = reinterpret_cast<RTP*>(&buf[i]);
//        
//        if(p2->ssi == 0x0447e48d){
//            uint32_t tm = ntohl(p2->time)/100;
//            uint8_t ver = p2->ver;
//            uint8_t cc = p2->cc;
//            uint8_t type = p2->type;
//            uint16_t seq = ntohs(p2->seq);
//            
//            int pack_sz = &buf[i] - p1;
//            
//            unsigned char out_buf[102400];
//            size_t done = 0;
//            int err = mpg123_decode(mh, reinterpret_cast<const unsigned char*>(p1), (size_t)pack_sz, out_buf, 10240, &done);
//            if(err && err != MPG123_NEED_MORE && err != MPG123_NEW_FORMAT){//
//                std::cerr << mpg123_plain_strerror(err) << std::endl;
//                break;
//            }
//            
//            if(err == MPG123_NEW_FORMAT){
//                long rate;
//                int channels;
//                int encoding;
//                err = mpg123_getformat(mh, &rate, &channels, &encoding);
//                if(err){
//                    std::cerr << mpg123_plain_strerror(err) << std::endl;
//                    break;
//                }
//                
//                std::cout << "New format " << rate << " " << channels << std::endl;
//                player = new MyPlayer(rate, channels);
//            }else if((err==MPG123_OK || err==MPG123_NEED_MORE) && done){
//                usleep(26000);
//                
//                if(player){
//                    if(done <= player->freeFrames()){
//                        std::cout << "PLAY " << done << std::endl;
//                        player->play((int16_t*)out_buf, done/4);
//                    }else{
//                        std::cerr << "SKIP " << done << " " << player->freeFrames();
//                    }
//                }
//            }
//            
//            p1 = &buf[i]+16;
//        }
//    }
//    
//}

void test3(){
    TCPChannel *tcp = new TCPChannel("client1");
    tcp->setServerParams("192.168.1.197", 4471);
    tcp->connectServer();
    
    while(1){
        tcp->run();
    }
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% newsimpletest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

//    std::cout << "%TEST_STARTED% test1 (newsimpletest)" << std::endl;
//    test1();
//    std::cout << "%TEST_FINISHED% time=0 test1 (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% test2 (newsimpletest)\n" << std::endl;
    test3();
    std::cout << "%TEST_FINISHED% time=0 test2 (newsimpletest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

