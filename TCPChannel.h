/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TCPChannel.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 1:04
 */

#ifndef TCPCHANNEL_H
#define TCPCHANNEL_H

#include <string>
#include "Channel.h"

using namespace std;

class TCPChannel : public Channel {
public:
    enum State{
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        PLAY
    };
    string clientName;
    State state;
    string targetState;
    
    TCPChannel();
    TCPChannel(string clientName);
    TCPChannel(const TCPChannel& orig);
    virtual ~TCPChannel();
    
    void setServerParams(string address, uint16_t port);
    bool isConfigured();
    void unconfig();
    void connectServer();
    void setState(string state);
    virtual void onRead();
    virtual void onError();
private:
    int toConnecting;
    string address = "";
    uint16_t port = 0;
    bool configured = true;

    void parseCommand(string data);
    void onConnected();
    void onDisconnected();
    virtual void onTime();
    virtual void sendData(string data);
    virtual void sendDataTo(string data, sockaddr_in addr);
};

#endif /* TCPCHANNEL_H */

