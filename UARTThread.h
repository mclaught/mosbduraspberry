/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.h
 * Author: user
 *
 * Created on 16 марта 2017 г., 10:55
 */

#ifndef UARTTHREAD_H
#define UARTTHREAD_H

#include <termios.h>
#include <mutex>
#include <vector>

#include "MyThread.h"

#define LINE_ON 12
#define BKS_EN 13

typedef uint16_t targets_t;

using namespace std;

class UARTThread : public MyThread {
public:
    UARTThread();
    UARTThread(const UARTThread& orig);
    virtual ~UARTThread();
    
    bool update;
    targets_t targets;
    targets_t old_targets;
    bool good;
    
    void setTargets(targets_t _targets);
    targets_t getTargets();
private:
    int fd;
    mutex mut;
    uint64_t cur_tm;
    bool sound_on;
    uint64_t tm_sound_on;
    vector<int> target_pins;
    
    int set_interface_attribs (int fd, int speed, int parity);
    bool set_blocking (int fd, int should_block);
    virtual void exec();
    void process_state(uint16_t state);
    uint64_t getMillis();
    void soundOn(bool new_sound_on);
};

extern UARTThread* uart_thread;

#endif /* UARTTHREAD_H */

