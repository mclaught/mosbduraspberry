/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.cpp
 * Author: user
 * 
 * Created on 16 марта 2017 г., 10:55
 */
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <wiringPi.h>
#include "log.h"
#include "UARTThread.h"
#include "Settings.h"

UARTThread* uart_thread;

UARTThread::UARTThread() : MyThread()
{
    good = false;
    char portname[] = "/dev/ttyAMA0";
    
    fd = open ((const char*)portname, O_RDWR | O_NOCTTY);
    if (fd < 0)
    {
            cerr << red << "error " << errno << " opening " << portname << " " << strerror (errno) << clrst << endl;
            return;
    }

    if(set_interface_attribs (fd, B2400, 0) < 0)  // set speed to 115,200 bps, 8n1 (no parity)
        return;
    if(!set_blocking (fd, 1))
        return;
    
    update = true;
    good = true;
    targets = 0xFFFF;
    old_targets = 0;
    tm_sound_on = 0;
    sound_on = false;
    
    target_pins = setts.read_ints("target-pins");
    if(target_pins.size() == 0){
        for(int i=0; i<4; i++)
            target_pins.push_back(i);
    }
    for(int pin : target_pins){
        pinMode(pin, OUTPUT);
        cout << magenta << "Pin " << pin << clrst << endl;
    }
    
    cout << magenta << "UART thread created" << clrst << endl;
}

UARTThread::UARTThread(const UARTThread& orig)
{
}

UARTThread::~UARTThread()
{
    close(fd);
}

int UARTThread::set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << red << "error " << errno << " from tcgetattr" << clrst << endl;
            return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
            cerr << red << "error " << errno << " from tcsetattr" << clrst << endl;
            return -1;
    }
    return 0;
}

bool UARTThread::set_blocking (int fd, int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << red << "error " << errno << " from tcgetattr" << clrst << endl;
            return false;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        cerr << red << "error " << errno << " from tcsetattr: " << strerror(errno) << clrst << endl;
        return false;
    }
    
    return true;
}

void UARTThread::setTargets(targets_t _targets)
{
    std::unique_lock<mutex> locker(mut);
    
    targets = ~_targets;
    update = true;
    tm_sound_on = cur_tm;
    
}

targets_t UARTThread::getTargets(){
    return targets;
}

uint64_t UARTThread::getMillis()
{
    timeval tv;
    gettimeofday(&tv,0);
    return tv.tv_sec*1000 + tv.tv_usec/1000;
}

void UARTThread::soundOn(bool new_sound_on)
{
    digitalWrite(LINE_ON, LOW);
    cout << magenta << "Sound off" << clrst << endl;
    usleep(10000);

    if(new_sound_on)
    {
        digitalWrite(LINE_ON, HIGH);
        cout << magenta << "Sound on" << clrst << endl;
        usleep(15000);
    }

    uint8_t targ_hi = (targets & 0xFF00) >> 8;
    uint8_t targ_lo = (targets & 0x00FF);

    if(write(fd, (void*)&targ_hi, 1) < 0)
        cerr << red << "error " << errno << " from write: " << strerror(errno) << clrst << endl;
    if(tcflush(fd,TCIOFLUSH) < 0)
        cerr << red << "error " << errno << " from fsync: " << strerror(errno) << clrst << endl;
    usleep(10000);
    if(write(fd, (void*)&targ_lo, 1) < 0)
        cerr << red << "error " << errno << " from write: " << strerror(errno) << clrst << endl;
    if(tcflush(fd,TCIOFLUSH) < 0)
        cerr << red << "error " << errno << " from fsync: " << strerror(errno) << clrst << endl;

    old_targets = targets;

    cout << magenta << "Mask: ";
    for(int i=0; i<16; i++){
        if(i < target_pins.size() && target_pins[i] != -1){
            if(targets & (1<<i)){
                digitalWrite(target_pins[i], LOW);
                cout << "=";
            }else{
                digitalWrite(target_pins[i], HIGH);
                cout << "*";
            }
        }else{
            if(targets & (1<<i)){
                cout << "-";
            }else{
                digitalWrite(target_pins[i], HIGH);
                cout << "+";
            }
        }
        
    }
    cout << clrst << endl;
    
    for(int i=0; i<16; i++){
    }
}

void UARTThread::exec()
{
    while(!terminated)
    {
//        targets = 0xA5;
//        write(fd, (void*)&targets, sizeof(targets));
        
        cur_tm = getMillis();
        
        if(update)// sound_on && targets != old_targets
        {
            std::unique_lock<mutex> locker(mut);

            soundOn(targets != 0xffff);
            
            update = false;
            old_targets = targets;
        }
        
//        usleep(100000);
        
//        {
//            std::unique_lock<mutex> locker(mut);
//            uint64_t d_tm = cur_tm - tm_sound_on;
//            bool new_sound_on = (d_tm < 200);
//            if(new_sound_on != sound_on)
//            {
//                //cout << "soundOn(" << new_sound_on << ")" << endl;
//                soundOn(new_sound_on);
//            }
//            sound_on = new_sound_on;
//        }
        
//        usleep(100000);
    }
}

void UARTThread::process_state(uint16_t state)
{
    cout << magenta << "state = " << state << clrst << endl;
}