/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPChannel.cpp
 * Author: user
 * 
 * Created on 19 сентября 2018 г., 23:28
 */

#include <netinet/in.h>
#include <arpa/inet.h>
#include "UDPChannel.h"
#include <iostream>
#include "log.h"

UDPChannel::UDPChannel() : Channel() {
}

UDPChannel::UDPChannel(string iface, string multicastAddr, string clientName) : Channel(){
    name = "UDPChannel";
    error = "";
    this->iface = iface;
    this->multicastAddr = multicastAddr;
    this->clientName = clientName;
    
    printf("\033[32mUDPChannel starting. Multicat address %s...\033[0m", multicastAddr.c_str());

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1){
        cerr << red << "UDP socket: " << strerror(errno) << clrst << endl;
        exit(EXIT_FAILURE);
    }
    
    sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(4674)
    };
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1){
        cerr << red << "UDP bind: " << strerror(errno) << clrst << endl;
        exit(EXIT_FAILURE);
    }
    
    struct ip_mreq mreq;
    inet_aton(multicastAddr.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        cerr << red << "UDP membership: " << strerror(errno) << clrst << endl;
        exit(EXIT_FAILURE);
    }
    
    cout << green << "OK" << clrst << endl;

    active = true;
}

UDPChannel::UDPChannel(const UDPChannel& orig) {
}

UDPChannel::~UDPChannel() {
    struct ip_mreq mreq;
    inet_aton(multicastAddr.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        cerr << red << "UDP drop membership: " << strerror(errno) << clrst << endl;
        return;
    }
}

void UDPChannel::searchServer(){
    
    if(!doSearch){
        Channel::CmdParams params;
        params["client"] = clientName;
        params["command"] = "recognize";
        sendRequest(params);
    }
    doSearch = true;
}

void UDPChannel::stopSearch(){
}

void UDPChannel::sendData(string data){
    std::cout << green << "Send udp: " << data << clrst << std::endl;
    
    sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(4672)
    };
    inet_aton(multicastAddr.c_str(), &addr.sin_addr);
    
    int err = sendto(sock, data.c_str(), data.length(), 0, (sockaddr*)&addr, sizeof(sockaddr_in));
    if(err == -1){
        cerr << red << strerror(errno) << clrst << endl;
    }
    
}

void UDPChannel::sendDataTo(string data, sockaddr_in addr){
    std::cout << green << dec << "Send udp to " << inet_ntoa(addr.sin_addr) << ":" << ntohs(addr.sin_port) << data << clrst << std::endl;
    
    int err = sendto(sock, data.c_str(), data.length(), 0, (sockaddr*)&addr, sizeof(sockaddr_in));
    if(err == -1){
        cerr << red << strerror(errno) << clrst << endl;
    }
}

void UDPChannel::onRead(){
    char data[1024];
    sockaddr_in from_addr;
    socklen_t addr_len = sizeof(sockaddr_in);
    memset(&from_addr, 0, sizeof(sockaddr_in));
    size_t sz = recvfrom(sock, data, 1024, MSG_DONTWAIT, (sockaddr*)&from_addr, &addr_len);
    if(sz == -1){
        if(errno != EAGAIN)
            cerr << red << strerror(errno) << clrst << endl;
        return;
    }

    std::cout << green << dec << "UDP read " << inet_ntoa(from_addr.sin_addr) << ":" << ntohs(from_addr.sin_port) << ": " << data << clrst << std::endl;

    XMLDocument xml;
    if(xml.Parse(data, sz) == XMLError::XML_SUCCESS){
        XMLElement* root = xml.RootElement();
        if(root){
            string name = root->Name();
            if(name == "RESPONSE" && checkCommand(root, "recognize", "")){
                bool ok = false;
                char* sServerPort = const_cast<char*>(root->Attribute("tcp_port"));
                uint16_t serverPort = atoi(sServerPort);
                if(serverPort){
                    stopSearch();

                    from_addr.sin_port = htons(serverPort);
                    doSearch = false;
                    sendMessage(MSG_SERVER_FOUND, (void*)&from_addr);
                }
            }else if(name == "REQUEST" && checkCommand(root, "recognize", "")){
                CmdParams params;
                params["client"] = clientName;
                params["type"] = "target_in";
                params["subtype"] = "bdu";
                sendResponse(root, from_addr, params);
            }
        }
    }else{
        std::cerr << red << "XML error: " << xml.ErrorName() << " " << xml.ErrorStr() << clrst << std::endl;
    }
}

void UDPChannel::onTime(){
    if(doSearch){
        Channel::CmdParams params;
        params["client"] = clientName;
        params["command"] = "recognize";
        sendRequest(params);
    }
}

void UDPChannel::onError(){
    
}
