/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyPlayer.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 12:50
 */

#include "MyPlayer.h"
#include <iostream>

using namespace std;

MyPlayer *player = nullptr;

MyPlayer::MyPlayer() {
}

MyPlayer::MyPlayer(int rate, int channels) : sample_rate(rate), channels(channels){
    create_alsa();
}

MyPlayer::MyPlayer(const MyPlayer& orig) {
}

MyPlayer::~MyPlayer() {
}

bool MyPlayer::create_alsa()
{
    snd_pcm_hw_params_t *hw_params;
    snd_pcm_sw_params_t *sw_params;
    buffer_size = sample_rate;
    snd_pcm_uframes_t period_size = 4608/4;

    int err = snd_pcm_open(&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (err < 0)
    {
        cerr << "snd_pcm_open: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0)
    {
        cerr << "snd_pcm_hw_params_malloc: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_any(playback_handle, hw_params)) < 0)
    {
        cerr << "snd_pcm_hw_params_any: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_access(playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_access: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_format(playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_format: " << snd_strerror(err) << endl;
        return false;
    }

    unsigned int sr = sample_rate;
    if ((err = snd_pcm_hw_params_set_rate_near(playback_handle, hw_params, (unsigned int*) &sr, 0)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_rate_near: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_channels(playback_handle, hw_params, channels)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_channels: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_buffer_size_near(playback_handle, hw_params, &buffer_size)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_buffer_size_near: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params_set_period_size_near(playback_handle, hw_params, &period_size, NULL)) < 0)
    {
        cerr << "snd_pcm_hw_params_set_period_size_near: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_hw_params(playback_handle, hw_params)) < 0)
    {
        cerr << "snd_pcm_hw_params: " << snd_strerror(err) << endl;
        return false;
    }

    snd_pcm_hw_params_free(hw_params);

    //Software params
    if ((err = snd_pcm_sw_params_malloc(&sw_params)) < 0)
    {
        cerr << "snd_pcm_sw_params_malloc: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_sw_params_current(playback_handle, sw_params)) < 0)
    {
        cerr << "snd_pcm_sw_params_current: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_sw_params_set_start_threshold(playback_handle, sw_params, buffer_size - period_size)) < 0)
    {
        cerr << "snd_pcm_sw_params_set_start_threshold: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_sw_params_set_avail_min(playback_handle, sw_params, period_size)) < 0)
    {
        cerr << "snd_pcm_sw_params_set_avail_min: " << snd_strerror(err) << endl;
        return false;
    }

    if ((err = snd_pcm_sw_params(playback_handle, sw_params)) < 0)
    {
        cerr << "snd_pcm_sw_params: " << snd_strerror(err) << endl;
        return false;
    }

    snd_pcm_sw_params_free(sw_params);

    if ((err = snd_pcm_prepare(playback_handle)) < 0)
    {
        cerr << "snd_pcm_prepare: " << snd_strerror(err) << endl;
        return false;
    }

    return true;
}

void MyPlayer::play(int16_t* pcm, int frm_sz)
{
    if (!alsa_ok)
        alsa_ok = create_alsa();

    if(alsa_ok)
    {
//        for(int i=0; i<frm_sz; i++)
//            pcm[i] <<= 1;
        
        int error;
        if ((error = snd_pcm_writei(playback_handle, pcm, frm_sz)) != frm_sz)
        {
            snd_pcm_recover(playback_handle,error,true);
            std::cout << "ALSA recover" << endl;
        }
    }
}

int MyPlayer::freeFrames(){
    return snd_pcm_avail(playback_handle);
}

int MyPlayer::snd_pcm_recover(snd_pcm_t *pcm, int err, int silent)
{
        if (err > 0)
                err = -err;
        if (err == -EINTR)	/* nothing to do, continue */
                return 0;
        if (err == -EPIPE) {
                const char *s = "underrun";
//                if (snd_pcm_stream(pcm) == SND_PCM_STREAM_PLAYBACK)
//                        s = "underrun";
//                else
//                        s = "overrun";
                if (!silent)
                        SNDERR("%s occurred", s);
                err = snd_pcm_prepare(pcm);
                if (err < 0) {
                        SNDERR("cannot recovery from %s, prepare failed: %s", s, snd_strerror(err));
                        return err;
                }
                return 0;
        }
        if (err == -ESTRPIPE) {
                while ((err = snd_pcm_resume(pcm)) == -EAGAIN)
                        /* wait until suspend flag is released */
                        poll(NULL, 0, 1000);
                if (err < 0) {
                        err = snd_pcm_prepare(pcm);
                        if (err < 0) {
                                SNDERR("cannot recovery from suspend, prepare failed: %s", snd_strerror(err));
                                return err;
                        }
                }
                return 0;
        }
        return err;
}

MyPlayer* MyPlayer::create(int rate, int channels){
    if(player == nullptr){
        cout << "Create player" << endl;
        player = new MyPlayer(rate, channels);
    }
    return player;
}