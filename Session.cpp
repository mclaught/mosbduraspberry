/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 11:41
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sstream>
#include <sys/types.h> 
#include <sys/wait.h>
#include "Session.h"
#include "log.h"

Session::Session() : Channel(){
    
}

Session::Session(uint32_t id, string url, string iface) : Channel() {
    this->id = id;
    this->iface = iface;
    this->url = url;
    this->targetsMask = 0;
    
    shared_buf = (shared_buf_t*)mmap(NULL, sizeof(shared_buf_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0);
//    shared_buf->targets = 0;
    shared_buf->active = true;
    
    gettimeofday(&recv_tm,0);
}

void Session::init(){
    active = true;
    
    ostringstream name_stm;
    name_stm << "Session(" << id << ")";
    name = name_stm.str();
    std::cout << yellow << "Create socket session " << url << clrst << std::endl;
    std::cout << yellow << "Iface: " << iface << clrst << std::endl;
    
    if(!parseUrl(url, host, port)){
        cerr << red << "Error URL parsing" << clrst << endl;
    }
    
    cout << yellow << host << " : " << port << clrst << endl;
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1){
        cerr << red << "Session socket: " << strerror(errno) << clrst << endl;
        return;
    }
    
    sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(port)
    };
//    if(iface != "")
//        inet_aton(iface.c_str(), &(addr.sin_addr));
//    else
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1){
        cerr << red << "Session bind: " << strerror(errno) << clrst << endl;
        return;
    }
    
    struct ip_mreq mreq;
    inet_aton(host.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        cerr << red << "Session membership: " << strerror(errno) << clrst << endl;
        return;
    }
    
    mpgError("Init", mpg123_init());

    int error;
    mh = mpg123_new(nullptr, &error);
    mpgError("New decoder", error);
    
    mpgError("Open feed", mpg123_open_feed(mh));
    
    shared_buf->active = true;
    
    cout << yellow << "Session created" << clrst << endl;
}

void Session::close_me(){
    active = false;
    
    struct ip_mreq mreq;
    inet_aton(host.c_str(), &(mreq.imr_multiaddr));
    if(iface != "")
        inet_aton(iface.c_str(), &(mreq.imr_interface));
    else
        mreq.imr_interface.s_addr = INADDR_ANY;
    if( setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 ){
        cerr << red << "\033[31mSession drop membership: " << strerror(errno) << clrst << endl;
        return;
    }
    
    close(sock);
    mpg123_close(mh);
    mpg123_delete(mh);
    
    munmap(shared_buf, sizeof(shared_buf_t));
    
    cout << yellow << "Session closed" << clrst << endl;
}

bool Session::start(){
    pid = fork();
    
    if(pid == 0){
        shared_buf->active = true;
        init();
        while(shared_buf->active){//
            run();
            
            timeval tv;
            gettimeofday(&tv,0);
            if((tv.tv_sec - recv_tm.tv_sec) > SESSION_TIMEOUT){
                shared_buf->active = false;
                cout << yellow << "Timeout" << clrst << endl;
            }
        }
        close_me();
        exit(0);
    }else if(pid < 0){
        cerr << red << "Fork:" << strerror(errno) << clrst << endl;
    }
    cout << "New process " << pid << endl;
    
    return pid>0;
}

void Session::stop(){
    shared_buf->active = false;
    if(pid > 0)
        waitpid(pid, NULL, 0);
    cout << "Process stoped " << pid << endl;
}

Session::Session(const Session& orig) {
}

Session::~Session() {
}

bool Session::parseUrl(string url, string &host, uint16_t &port){
    int p = url.find("//");
    if(p == string::npos)
        return false;
    
    string host_port = url.substr(p+2);
    p = host_port.find(":");
    if(p != string::npos){
        host = host_port.substr(0, p);
        string sPort = host_port.substr(p+1);
        port = atoi(sPort.c_str());
    }else{
        return false;
    }
    return true;
}

void Session::sendData(string data){
    
}

void Session::sendDataTo(string data, sockaddr_in addr){}

void Session::onTime(){
    
}

void Session::onRead(){
    char data[1024];
    sockaddr_in from_addr;
    memset(&from_addr, 0, sizeof(sockaddr_in));
    from_addr.sin_family = AF_INET;
    socklen_t addr_len = sizeof(sockaddr_in);
    
    ssize_t sz = recvfrom(sock, data, 1024, MSG_DONTWAIT, (sockaddr*)&from_addr, &addr_len);
    if(sz == -1){
        cerr << red << strerror(errno) << clrst << endl;
        return;
    }
    
    if(sz){
        gettimeofday(&recv_tm,0);
        
        //декодирование, воспроизведение
        RTP* rtp = reinterpret_cast<RTP*>(data);

        char pcm[PCM_SZ];
        size_t pcm_sz = 0;
        int err = mpg123_decode(mh,
                                reinterpret_cast<const unsigned char*>(rtp->payload),
                                size_t(sz-16),
                                reinterpret_cast<unsigned char*>(pcm),
                                PCM_SZ,
                                &pcm_sz);
        //std::cout << dec << "Decoded " << pcm_sz << " err=" << err << endl;
        
        uint32_t tm = ntohl(rtp->time)/100;
        if(start_tm == 0)
            start_tm = tm;
        
        if(err == MPG123_NEW_FORMAT){
            std::cout << yellow << "NEW FORMAT: " << clrst;

            int encoding;
            if(mpgError("Get format", mpg123_getformat(mh, &rate, &channels, &encoding))){
                return;
            }
            std::cout << yellow << rate << " " << channels << clrst << std::endl;

            //Set audio format
            player = MyPlayer::create(rate, channels);
        }else if(err==MPG123_OK || err == MPG123_NEED_MORE){
            //Write PCM data to device
            if(player){
                int avail = player->freeFrames();
                if(pcm_sz/2/channels <= avail){
                    std::cout << yellow << dec << "PLAY " << (tm-start_tm) << " " << avail << clrst << "\r";//std::endl;
                    player->play((int16_t*)pcm, pcm_sz/channels/2);
                }else{
                    std::cout << yellow << dec << "\t\tSKIP " << pcm_sz << " " << avail << clrst << "\r";
                }
            }
        }else{
            mpgError("Decode", err);
        }
    }
}

bool Session::mpgError(string pref, int error)
{
    if(error != 0)
        std::cerr << red << pref << ": " << mpg123_plain_strerror(error) << clrst << std::endl;
    return error != 0;
}
//
//int Session::findTargetId(string tgt){
//    int i = 0;
//    for(auto t : targetsList){
//        if(t == tgt)
//            return i;
//        i++;
//    }
//    return -1;
//}

void Session::addTarget(int tgt){
    targetsMask |= (1 << tgt);//shared_buf->targets
//    int tgtId = findTargetId(tgt);
//    if(tgtId != -1){
//        //targets.insert(tgt);
//        targetsMask |= (1 << tgtId);
//    }
}

void Session::removeTarget(int tgt){
    targetsMask &= ~(1 << tgt);//shared_buf->targets
//    int tgtId = findTargetId(tgt);
//    if(tgtId != -1){
//        //targets.erase(tgt);
//        targetsMask &= ~(1 << tgtId);
//    }
}

void Session::addTargets(targets_t tgt_mask){
    targetsMask |= tgt_mask;//shared_buf->targets
}

void Session::removeTargets(targets_t tgt_mask){
    targetsMask &= ~tgt_mask;//shared_buf->targets
}

targets_t Session::getTargets(){
    return targetsMask;//shared_buf->targets
}

void Session::run(){
    fd_set rd_set, wr_set, *pwr_set = nullptr;
    FD_SET(sock, &rd_set);
    timeval tv = {0, 50000};

    int cnt = select(sock+1, &rd_set, nullptr, nullptr, &tv);
    if(cnt == -1){
        cerr << name << " select: " << strerror(errno) << endl;
    }else if(cnt > 0){
        if(FD_ISSET(sock, &rd_set)){
            onRead();
        }
    }
}

void Session::onError(){
}