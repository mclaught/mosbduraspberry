/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TCPChannel.cpp
 * Author: user
 * 
 * Created on 20 сентября 2018 г., 1:04
 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "TCPChannel.h"
#include <iostream>
#include <sys/ioctl.h>
#include <string>
#include <unistd.h>
#include <fcntl.h>
#include "log.h"

TCPChannel::TCPChannel() : Channel() {
}

bool SetSocketBlockingEnabled(int fd, bool blocking)
{
   if (fd < 0) return false;

   int flags = fcntl(fd, F_GETFL, 0);
   if (flags == -1) return false;
   flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
   return fcntl(fd, F_SETFL, flags) != -1;
}

TCPChannel::TCPChannel(string clientName) : Channel(), clientName(clientName){
    name = "TCPChannel "+clientName;
    state = DISCONNECTED;
    targetState = "free";

    std::cout << cyan << "TCP channel created for " << clientName << clrst << std::endl;
}

bool TCPChannel::isConfigured(){
    return configured;
}

void TCPChannel::unconfig(){
    configured = false;
}

void TCPChannel::setServerParams(string address, uint16_t port){
    this->address = address;
    this->port = port;
    this->configured = true;
}

void TCPChannel::connectServer()
{
//    if(port == 0){//!isConfigured()
//        //cerr << "No server params specified" << endl;
//        return;
//    }
    
    cout << cyan << "TCP connecting to " << address << ":" << port << "..." << clrst;
    
    state = CONNECTING;
    
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        cerr << red << strerror(errno) << clrst << endl;
        onDisconnected();
        return;
    }
    
//    if(!SetSocketBlockingEnabled(sock, false)){
//        cerr << "SetSocketBlockingEnabled: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    inet_aton(address.c_str(), &addr.sin_addr);
    addr.sin_port = htons(port);
    
    if(connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)) == -1 && errno != EINPROGRESS){
        cerr << red << "connect: " << strerror(errno) << clrst << endl;
        unconfig();
        onDisconnected();
        return;
    }
    
    cout << cyan << "Connected " << clientName << clrst << endl;
    
//    struct timeval tv = {5,0};
//    fd_set w_fds;
//    errno = EINPROGRESS;
//    while(errno == EINPROGRESS){
//        FD_ZERO(&w_fds);
//        FD_SET(sock, &w_fds);
//        if(select(sock+1, NULL, &w_fds, NULL, &tv) == -1 && errno != EINPROGRESS){
//            cerr << "select: " << strerror(errno) << endl;
//            //if(errno != EINPROGRESS)
//            port = 0;
//            onDisconnected();
//            return;
//        }
//    }
//    
//    if(!FD_ISSET(sock, &w_fds)){
//        cerr << "Timeout" << endl;
//        port = 0;
//        onDisconnected();
//        return;
//    }
//    
//    int err = 0;
//    socklen_t len = sizeof(err);
//    if(getsockopt(sock, SOL_SOCKET, SO_ERROR, &err, &len)){
//        cerr << "getsockopt: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
//    
//    if(err){
//        cerr << "err: " << strerror(err) << endl;
//        port = 0;
//        onDisconnected();
//        return;
//    }
//    
//    if(!SetSocketBlockingEnabled(sock, true)){
//        cerr << "SetSocketBlockingEnabled: " << strerror(errno) << endl;
//        onDisconnected();
//        return;
//    }
    
    toConnecting = 2;
    
    onConnected();
}

TCPChannel::TCPChannel(const TCPChannel& orig) {
}

TCPChannel::~TCPChannel() {
}

void TCPChannel::setState(string state)
{
    std::cout << cyan << "New state: " << state << clrst << std::endl;
    targetState = state;

    CmdParams params;
    params["client"] = clientName;
    params["command"] = "state";
    params["action"] = "changed";
    params["state"] = state;
    sendRequest(params);
}

void TCPChannel::parseCommand(string data)
{
    XMLDocument xml;
    if(xml.Parse(data.c_str()) == XMLError::XML_SUCCESS){
        XMLElement* root = xml.RootElement();
        if(root){
            if(string(root->Name()) == "RESPONSE"){

                if(!root->Attribute("client", clientName.c_str()))
                    return;

                if(checkCommand(root, "connection", "connect")){
//                    state = CONNECTED;
//                    sendMessage(MSG_SERVER_CONNECTED, (void*)clientName.c_str());
                    if(root->Attribute("error")){
                        onDisconnected();
                    }
                }
            }

            if(string(root->Name()) == "REQUEST"){
                CmdParams addParams;
                sockaddr_in addr;
                sendResponse(root, addr, addParams);

                if(checkCommand(root, "receive", "start")){
                    uint32_t id = (uint32_t)root->IntAttribute("session");
                    string url = root->Attribute("url");
                    setState("run");
                    
                    SessionsPrms prms = {id, url, clientName};
                    sendMessage(MSG_START_SESSION, &prms);
                }

                if(checkCommand(root, "receive", "finish")){
                    uint32_t id = (uint32_t)root->IntAttribute("session");
                    setState("free");
                    
                    SessionsPrms prms = {id, "", clientName};
                    sendMessage(MSG_FINISH_SESSION, &prms);
                }
            }
        }
    }else{
        std::cerr << red << "TCP XML error: " << xml.ErrorStr() << clrst << std::endl;
        onDisconnected();
    }
}

void TCPChannel::sendData(string data){
    std::cout << cyan << "Send TCP: " << data << clrst << std::endl;
    if(send(sock, data.c_str(), data.length(), 0) == -1){
        cerr << red << "send: " << strerror(errno) << clrst << endl;
        onDisconnected();
    }
}

void TCPChannel::sendDataTo(string data, sockaddr_in addr){
    sendData(data);
}

void TCPChannel::onConnected()
{
    std::cout << cyan << clientName << " connected" << clrst << std::endl;
    
    active = true;
    state = CONNECTED;
    sendMessage(MSG_SERVER_CONNECTED, (void*)clientName.c_str());

    CmdParams params;
    params["client"] = clientName;
    params["command"] = "connection";
    params["action"] = "connect";
    params["type"] = "target_in";
    params["state"] = targetState;
    sendRequest(params);
}

void TCPChannel::onDisconnected()
{
    active = false;
    std::cout << cyan << clientName << " disconnected" << clrst << std::endl;
    state = DISCONNECTED;
    sendMessage(MSG_SERVER_DISCONNECTED, &clientName);
    close(sock);
    sock = 0;
}

void TCPChannel::onTime()
{
//    if(state == CONNECTING){
//        if(toConnecting > 0){
//            toConnecting--;
//        }else{
//            std::cerr << "! " << clientName << " timeout" << std::endl;
//            onDisconnected();
//        }
//    }

    if(state == CONNECTED){
        CmdParams params;
        params["client"] = clientName;
        params["command"] = "connection";
        params["action"] = "check";
        params["type"] = "target_in";
        params["state"] = targetState;
        sendRequest(params);
    }
}

void TCPChannel::onRead()
{
    char data[1024];
    
//    int available = 0;
//    if(ioctl(sock, FIONREAD, &available) == -1){
//        cerr << red << "ioctl: " << strerror(errno) << clrst << endl;
//        onDisconnected();
//        return;
//    }
    
    int l = recv(sock, data, 1024, MSG_DONTWAIT);
    if(l == -1){
        if(errno != EAGAIN){
            onDisconnected();
            cerr << red << "recv: " << strerror(errno) << clrst << endl;
        }
        return;
    }else if(l == 0){
        onDisconnected();
        return;
    }
    data[l] = 0;
    
    std::cout << cyan << "TCP read: " << data << clrst << std::endl;
    parseCommand(string(data));
}

void TCPChannel::onError(){
    cerr << red << "TCP channel error" << clrst << endl;
    onDisconnected();
}
