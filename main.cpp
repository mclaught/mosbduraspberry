/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 19 сентября 2018 г., 19:37
 */

#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <netinet/in.h>
#include "Session.h"
#include "TCPChannel.h"
#include "MainModel.h"
#include "mpg123-1.25.10/src/libmpg123/mpg123.h"
#include "MyPlayer.h"
#include <wiringPi.h>
#include "UARTThread.h"
#include "version.h"
#include "Settings.h"

using namespace std;

/*
 * 
 */
//void test2() {
//    long rate;
//    int channels;
//    
//    mpg123_init();
//
//    int error;
//    mpg123_handle* mh = mpg123_new(NULL, &error);
//    std::cout << mpg123_plain_strerror(error) << std::endl;
//    
//    FILE *f = fopen("test.bin", "rb");
//    if(!f){
//        std::cout << "%TEST_FAILED% time=0 testname=test2 (newsimpletest) message=File not found" << std::endl;
//    }
//    
//    int fsz = 6*1024*1024;
//    char buf[fsz];
//    char *p = buf;
//    fsz = fread(buf, 1, fsz, f);
////    fsz = 0;
////    while(!feof(f)){
////        int sz = fread(buf, 1, 1024, f);
////        p += sz;
////        fsz += sz;
////    }
//    if(fsz == 0){
//        std::cerr << strerror(ferror(f)) << std::endl;
//        return;
//    }
//    
//    int err = mpg123_open_feed(mh);
//    if(err){
//        std::cerr << mpg123_plain_strerror(err) << std::endl;
//        return;
//    }
//    
//    err = mpg123_volume(mh, 0.5);
//    if(err){
//        std::cerr << mpg123_plain_strerror(err) << std::endl;
//        return;
//    }
//    
//    uint32_t start_tm = 0;
//    char* p1 = buf+16;
//    
//    for(int i = 16; i<fsz; i++){
//        RTP* p2 = reinterpret_cast<RTP*>(&buf[i]);
//        
//        if(p2->ssi == 0x0447e48d){
//            uint32_t tm = ntohl(p2->time)/100;
//            uint8_t ver = p2->ver;
//            uint8_t cc = p2->cc;
//            uint8_t type = p2->type;
//            uint16_t seq = ntohs(p2->seq);
//            
//            if(start_tm == 0)
//                start_tm = tm;
//            
//            int pack_sz = &buf[i] - p1;
//            
//            unsigned char out_buf[102400];
//            size_t done = 0;
//            int err = mpg123_decode(mh, reinterpret_cast<const unsigned char*>(p1), (size_t)pack_sz, out_buf, 10240, &done);
//            if(err && err != MPG123_NEED_MORE && err != MPG123_NEW_FORMAT){//
//                std::cerr << mpg123_plain_strerror(err) << std::endl;
//                break;
//            }
//            
//            if(err == MPG123_NEW_FORMAT){
//                int encoding;
//                err = mpg123_getformat(mh, &rate, &channels, &encoding);
//                if(err){
//                    std::cerr << mpg123_plain_strerror(err) << std::endl;
//                    break;
//                }
//                
//                std::cout << "New format " << rate << " " << channels << std::endl;
//                player = new MyPlayer(rate, channels);
//            }else if((err==MPG123_OK || err==MPG123_NEED_MORE) && done){
//                usleep(24000);
//                
//                if(player){
//                    int avail = player->freeFrames();
//                    if(done/2/channels <= avail){
//                        //std::cout << "PLAY " << tm-start_tm << " " << avail << std::endl;
//                        player->play((int16_t*)out_buf, done/4);
//                    }else{
//                        std::cerr << "SKIP " << done << " " << avail << endl;
//                    }
//                }
//            }
//            
//            p1 = &buf[i]+16;
//        }
//    }
//    
//}
//
//void test3(){
//    TCPChannel *tcp = new TCPChannel("US-MUS.03.02.2F");
//    tcp->setServerParams("192.168.1.197", 4471);
//    tcp->connectServer();
//    
//    while(1){
//        tcp->run();
//    }
//}

string getSerial(){
    ifstream f;
    f.open("/proc/cpuinfo");
    string str;
    int step = 0;
    while(f.good() && !f.eof()){
        f >> str;
        
        switch(step){
            case 0:
                if(str.find("Serial")==0)
                    step = 1;
                break;
            case 1:
                if(str.find(":")!=string::npos)
                    step = 2;
                break;
            case 2:
                return str;
                break;
        }
    }
    return "---";
}

uint32_t calcCode(string sn){
    uint32_t code = 0;
    for(int i=0; i<sn.length(); i++){
        int n = i % 4;
        code ^= sn[i] << (n*8);
    }
    return code;
}

int main(int argc, char** argv) {
    if(argc > 1){
        for(int i=1; i<argc; i++){
            if(strncmp(argv[i], "-v", 2)==0){
                cout << "MosBDU service. Version: " << VERSION << endl;
            }
            if(strncmp(argv[i], "-s", 2)==0){
                cout << getSerial() << endl;
            }
        }
        return 1;
    }
    
    auto sn = getSerial();
    auto code = calcCode(sn);
    auto unlock = setts.read_int("unlock");
    //cout << "Serial=" << sn << " code=" << code << " unlock=" << unlock << endl;
    if(code != unlock)
        return 1;
    
    MainModel *model = new MainModel();
    
    wiringPiSetup();
    pinMode(LINE_ON, OUTPUT);
    pinMode(BKS_EN, INPUT);
    
    digitalWrite(LINE_ON, LOW);
    
    uart_thread = new UARTThread();
    if(!uart_thread->good){
        cerr << "UART thread error!" << endl;
        return 1;
    }
    
    uart_thread->start();
    
    while(1)
        model->run();

    return 0;
}

