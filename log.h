/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   log.h
 * Author: user
 *
 * Created on 20 октября 2018 г., 14:31
 */

#ifndef LOG_H
#define LOG_H

#define red "\033[31m"
#define green "\033[32m"
#define cyan "\033[36m"
#define yellow "\033[33m"
#define magenta "\033[1;35m"
#define clrst "\033[0m"

#define LOG(s) std::cout << clrst << s
#define LOGG(s) std::cout << green << s << clrst
#define LOGB(s) std::cout << cyan << s << clrst
#define LOGY(s) std::cout << yellow << s << clrst
#define ERR(s) std::cerr << red << s << clrst

#endif /* LOG_H */

