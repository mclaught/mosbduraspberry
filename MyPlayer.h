/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyPlayer.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 12:50
 */

#ifndef MYPLAYER_H
#define MYPLAYER_H

#include <alsa/asoundlib.h>

class MyPlayer {
public:
    MyPlayer();
    MyPlayer(int rate, int channels);
    MyPlayer(const MyPlayer& orig);
    virtual ~MyPlayer();
    
    void play(int16_t* pcm, int frm_sz);
    int freeFrames();
    
    static MyPlayer* create(int rate, int channels);
private:
    int sample_rate;
    int channels;
    snd_pcm_t *playback_handle;
    bool alsa_ok;
    snd_pcm_uframes_t buffer_size;
    
    bool create_alsa();
    int snd_pcm_recover(snd_pcm_t *pcm, int err, int silent);
};

#endif /* MYPLAYER_H */

