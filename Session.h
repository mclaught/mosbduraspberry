/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.h
 * Author: user
 *
 * Created on 20 сентября 2018 г., 11:41
 */

#ifndef SESSION_H
#define SESSION_H

#include <string>
#include <set>
#include "Channel.h"
#include "mpg123-1.25.10/src/libmpg123/mpg123.h"
#include "MyPlayer.h"
#include "UARTThread.h"

#define PCM_SZ 10240
#define SESSION_TIMEOUT 10

using namespace std;

#pragma pack(push, 1)
struct RTP{
    uint8_t cc:4;
    uint8_t x:1;
    uint8_t p:1;
    uint8_t ver:2;

    uint8_t type:7;
    uint8_t marker:1;

    uint16_t seq;
    uint32_t time;
    uint32_t ssi;
    uint32_t csrc;

    char payload[];
};
#pragma pack(pop)

struct shared_buf_t{
    //targets_t targets;
    bool active;
};

class Session : public Channel {
public:
    Session();
    Session(uint32_t id, string url, string iface);
    Session(const Session& orig);
    virtual ~Session();
    
    void init();
    void close_me();
    bool start();
    void stop();
    void addTarget(int tgt);
    void removeTarget(int tgt);
    void addTargets(targets_t tgt_mask);
    void removeTargets(targets_t tgt_mask);
    targets_t getTargets();
private:
    uint32_t id;
    string url;
    string iface;
    string host; 
    uint16_t port;
    mpg123_handle* mh;
    long rate;
    int channels;
    MyPlayer *player = nullptr;
    uint32_t start_tm = 0;
    //vector<string> targetsList;
    uint16_t targetsMask;
    shared_buf_t *shared_buf;
    timeval recv_tm;
    pid_t pid;
    
    virtual void sendData(string data);
    virtual void sendDataTo(string data, sockaddr_in addr);
    virtual void onTime();
    virtual void onRead();
    virtual void onError();
    virtual void run();
    bool mpgError(string pref, int error);
    bool parseUrl(string url, string &host, uint16_t &port);
//    int findTargetId(string tgt);
};

#endif /* SESSION_H */

