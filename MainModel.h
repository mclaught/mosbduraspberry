/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainModel.h
 * Author: user
 *
 * Created on 19 сентября 2018 г., 19:39
 */

#ifndef MAINMODEL_H
#define MAINMODEL_H

#include "MyObject.h"
#include "UDPChannel.h"
#include "TCPChannel.h"
#include "Session.h"
#include <set>
#include <map>
#include <vector>
#include <memory>

#define SMART_PTR

using namespace std;

#ifdef SMART_PTR
typedef shared_ptr<Session> PSession;
#else
typedef Session* PSession;
#endif

class MainModel : public MyObject {
public:
    MainModel();
    MainModel(const MainModel& orig);
    virtual ~MainModel();
    
    void run();
private:
    vector<string> targets;
    set<string> targets_set;
    typedef map<uint32_t, PSession> Sessions;
    string tgtName = "";
    
    UDPChannel *udp;
    set<TCPChannel*> tcps;
    vector<Channel*> channels;
    Sessions sessions;
    string iface = "";
    
    PSession findSession(uint32_t id);
    vector<string> splitString(string str);
    targets_t findTargets(string tgt);
    bool checkSocket(int sock);
};

#endif /* MAINMODEL_H */

