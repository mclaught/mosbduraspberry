#!/bin/bash

sudo apt-get update
sudo apt-get -y install gcc g++ make gdb libasound2-dev libmpg123-dev git-core mc
cd ~
sudo git clone git://git.drogon.net/wiringPi
cd ~/wiringPi
sudo ./build
cd ~

sudo git clone https://mclaught:svtapi71@gitlab.com/mclaught/mosbduraspberry.git
cd ~/mosbduraspberry
sudo make

sudo cp dist/Debug/GNU-Linux/mosbdu /usr/sbin

sudo mkdir /etc/mosbdu
sudo chmod 777 /etc/mosbdu
sudo echo 225.0.10.150 > /etc/mosbdu/udp-multicast

sudo cp mosbdu.d /etc/init.d
sudo chmod 755 /etc/init.d/mosbdu.d
sudo update-rc.d mosbdu.d defaults
sudo service mosbdu.d start

sudo cp -f cmdline.txt /boot
sudo cp -f config.txt /boot

sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime

#passwd

#sudo reboot

echo All done!!!
