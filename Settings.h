/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Settings.h
 * Author: user
 *
 * Created on 21 сентября 2018 г., 12:42
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <streambuf>
#include <vector>

using namespace std;

class Settings {
public:
    Settings();
    Settings(const Settings& orig);
    virtual ~Settings();
    
    string read_string(string name);
    void write_string(string name, string value);
    
    vector<string> read_strings(string name);
    void write_strings(string name, vector<string> values);
    
    int read_int(string name);
    void write_int(string name, int value);
    
    vector<int> read_ints(string name);
    void write_ints(string name, vector<int> ints);
private:
    string path;

};

extern Settings setts;

#endif /* SETTINGS_H */

