#!/bin/sh
### BEGIN INIT INFO
# Provides:          mosbdu
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: Moscow metro BDU
# Description:       Moscow metro BDU
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="Moscow metro BDU"
NAME=mosbdu
DAEMON=/usr/sbin/${NAME}
IFCONFIG=/boot/mosbdu/ifconfig

case $1 in
    start|restart)
        ifconfig > $IFCONFIG

        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        killall ${NAME} -s 9
        .${DAEMON} &
        ;;

    stop)
        echo "Stoping ${DESC}..."
        killall ${NAME} -s 9
        ;;

    status)
        pidof ${DAEMON}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi
        ;;
esac

